// Stable Code Version 1.1b; Date 17/06.2021;1.28AM
// Added rtos for keep wifi alive


#include <Adafruit_ADS1X15.h>
#include "EmonLib.h"
#include <driver/adc.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_Sensor.h>
#include "DHTesp.h"
#include <Ticker.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include "FS.h"
#include "SPIFFS.h"
#include <ArduinoJson.h>

void updateDisplay( void *pvParameters );

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif
#define FORMAT_SPIFFS_IF_FAILED true
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
#define ADC_INPUT 34
#define ADC_COUNTS  (1<<ADC_BITS)
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))
#define WIFI_TIMEOUT 20000 // 20 seconds




StaticJsonDocument<128>doc;
// JsonArray data = doc.createNestedArray("data");
JsonObject obj = doc.as<JsonObject>();

//Instances
EnergyMonitor emon1;
Adafruit_ADS1115 ads;
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
Adafruit_I2CDevice i2c_dev = Adafruit_I2CDevice(0x48);
DHTesp dht;




const int sample_size = 111, dhtPin = 4, led = 5;
String str_2 = "", str_E = "", Unit = "", Str_V = "", Str_I = "", Str_HI = "", json_data ="";
float heatIndex, ACvolts, energy_value = 0, prev_energy_value = 0, current_read = 1, I_avg[50] ;
unsigned long interval, start_time = 0;
int flag = 0, n=0;
TempAndHumidity newValues;

const char *ssid = "Shutter_Island";
const char *password = "Kesx5NBP";
const char *topic = "energymeter";
const char *mqtt_server = "192.168.1.60";
const int mqttPort = 1883;
const char *mqtt_username = "esp32Pico";
const char *mqtt_password = "esp32Pico";
unsigned long lastMsg = 0;
char msg[128];

int value = 0;
WiFiClient espClient;
PubSubClient client(espClient);



void initWiFi()
{
  Serial.println(ssid);
  // attempt to connect to WiFi network

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32_Pico";
    // Attempt to connect
    Serial.println(clientId);
if (client.connect(clientId.c_str(), mqtt_username, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}



void readFile(fs::FS &fs, const char * path, String &file_read) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path);
  if (!file || file.isDirectory()) {
    Serial.println("- failed to open file for reading");
    return ;
  }

  Serial.println("- read from file:");
  while (file.available()) {
    file_read = file.readStringUntil('\n');
    Serial.println(file_read);
  }
}
void writeFile(fs::FS &fs, const char * path, String &message) {
  Serial.printf("Writing file: %s\r\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("- failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("- file written");
  } else {
    Serial.println("- frite failed");
  }
}

float measure_current() {
  float amps = 0;
  amps = emon1.calcIrms(1480) - 0.015; // Calculate Irms only
  if (amps < 0.25) {
    return 0.0;
  }
  else
    return amps;
}

void error_led(int d, int t) {
  for (int i = 0; i < t; i++)
  {
    digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(d + 250);                     // wait for a second
    digitalWrite(led, LOW);    // turn the LED off by making the voltage LOW
    delay(d);
  }
}

void updateDisplay(void *pvParameters) {
    for(;;){

  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  display.print(" HI: " ); display.print(Str_HI); display.println(" Celsius");
  display.print(" V : " ); display.print(Str_V); display.println(" V");
  display.print(" I : "); display.print(Str_I); display.println(" A");
    display.print(" E : "); display.print(str_E); display.println(Unit);
  display.display();
  // Sleep for 2 seconds, then update display again!
  vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}





float volt_measure() {

  float  maxVal1 = 0, minVal1 = 0, pk_pk ;
  int i = 0;
  int16_t adc0[sample_size], maxVal0 = 0, minVal0 = 0 ;
  for (i = 0; i < sample_size; i++)
  {
    adc0[i] = ads.readADC_SingleEnded(0);
  }
  for (i = 0; i < sample_size; i++)
  {

    if (adc0[i] > maxVal0) {
      maxVal0 = adc0[i];
    }
  }
  minVal0 = maxVal0;
  for (i = 0; i < sample_size; i++)
  {
    if (adc0[i] < minVal0) {
      minVal0 = adc0[i];
    }
  }

  minVal1 = ads.computeVolts(minVal0);
  maxVal1 = ads.computeVolts(maxVal0);
  pk_pk = (maxVal1 -  minVal1) * 100.0001;
  if (pk_pk < 50)
  {
    return 0.0;
  }
  else
    return pk_pk;
}

float calc_energy(float V, float I, float p_e, unsigned long time_interval) {
  float e, p;
  e = ((V * I * time_interval) / (3600)) + p_e;
  flag = 0;
  if (e > 1100) {
    p = e / 1000;
    Serial.print("flag set " ); Serial.println("KWh");
    flag = 1;
    return p;
  }
  else {
    //e = 5 + prev_value.toFloat();
    Serial.print("Return Energy " ); Serial.println(e);
    return e;
  }
}

void deleteFile(fs::FS &fs, const char * path){
    Serial.printf("Deleting file: %s\r\n", path);
    if(fs.remove(path)){
        Serial.println("- file deleted");
    } else {
        Serial.println("- delete failed");
    }
}

void setup() {

  pinMode(led, OUTPUT);
  Serial.begin(115200);
      if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    //    Serial.println(F("SSD1306 allocation failed"));
    error_led (100, 5);
    ESP.restart();
  }
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0, 0);            // Start at top-left corner
  display.println("Initialising");
  display.display();
  if (!i2c_dev.begin()) {
    //    Serial.print("Did not find ADC Module");
    // Update the display
    display.clearDisplay();
    display.setCursor(0, 0);
    display.println("ADC Error!");
    display.display();
    while (1) {
      error_led(50, 50);
    }
  }
  else {
    display.println("ADC - OKAY");
    display.display();
  }
  dht.setup(dhtPin, DHTesp::DHT22);
  adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
  analogReadResolution(12);
  ads.begin();
  // Initialize emon library (30 = calibration number)
  emon1.current(ADC_INPUT, 30);

  if (!SPIFFS.begin(true)) {
    // Update the display
    display.clearDisplay();
    display.setCursor(0, 0);
    display.println("SPIFFS Error!");
    display.display();
    return;
  }
  else {
    display.println("SPIFFS - OKAY");
    display.display();
  }

  initWiFi();

  delay(1000);
  display.clearDisplay();
  display.setCursor(0, 0);
  display.println("WiFi - OKAY");
  display.print("IP:"); display.println(WiFi.localIP());
  display.display();
client.setServer(mqtt_server, mqttPort);
  reconnect();

  delay(1000);
  start_time = millis();
  xTaskCreatePinnedToCore(
  updateDisplay,
  "UpdateDisplay",  // Task name
  10000,            // Stack size (bytes)
  NULL,             // Parameter
  3,                // Task priority
  NULL,             // Task handle
  ARDUINO_RUNNING_CORE);
}



void loop() {
     if (!client.connected()) {
    reconnect();
    delay(1000);
  }

  int l = 0, m = 0;
  interval =  (millis() - start_time ) / 1000;
  if(n<1){
    current_read = 0;
    ACvolts =0;
  }
 
  
  if (i2c_dev.begin()) {
    display.clearDisplay();
    newValues = dht.getTempAndHumidity();
    heatIndex = dht.computeHeatIndex(newValues.temperature, newValues.humidity);
    ACvolts = volt_measure();

    
    for (l = 0; l < 50 ; l++) {
      I_avg[l] = measure_current();
    }
    for (m = 0; m < l; m++) {
      current_read += I_avg[m];
    }
  }
  current_read = current_read / m;
  if (current_read < 0.09 )
  {
    current_read = 0;
  }
  if (n<3)
  {
    /* code */
    current_read = 0;
    n++;
    Serial.println(n);
  }
  

  //  Serial.print("l = " ); Serial.println(l);
  //  Serial.print("m = " ); Serial.println(m);
  //Serial.print("Interval : " ); Serial.println(interval);
    readFile(SPIFFS, "/p1.txt", str_2);
    prev_energy_value = str_2.toFloat();
    Serial.print("prev_energy_value : " ); Serial.println(prev_energy_value);
    energy_value = calc_energy(ACvolts, current_read, prev_energy_value, interval);
    Serial.print("energy_value : " ); Serial.println(energy_value);
    str_E = String(energy_value, 2);
    deleteFile(SPIFFS, "/p1.txt");
    writeFile(SPIFFS, "/p1.txt", str_E);
     if (flag == 0) {
    Unit = "Wh";
  }
  else {
    Unit = "KWh";
  }
 
    error_led(30, 1);
    start_time = millis();
  
  Str_V=String(ACvolts, 1);
  Str_I = String(current_read, 2);
  Str_HI = String(heatIndex,2);
doc["v"] = Str_V.c_str();
doc["I"] = Str_I.c_str();
doc["HI"] = Str_HI.c_str();
doc["E"] = str_E.c_str();
serializeJson(doc, msg);



client.publish(topic, msg);
Serial.println(msg);
  



  delay(2000);
   client.loop();
  

}