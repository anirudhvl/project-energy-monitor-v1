# Project Energy Monitor V1

## Modules and components

1. Hardware components
    1. [ESP32Pico](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/hw-reference/esp32/get-started-pico-kit.html) KitV4
    2. ZMPT101B Voltage Sensor Module
    3. SCT-013-30 Current Transformer 
    4. ADS1115 ADC Module
    5. SSD1306 128X64 OLED Module
    6. DHT22 Sensor Module
2. Software Platforms
    1. PlatformIO Arduino environment
    3. [Free RTOS](https://www.arduino.cc/reference/en/libraries/freertos/)
    2. [EMQX v4.2.12](https://www.emqx.io/downloads) Open Source MQTT message Broker 
    3. [InfluxDB](https://portal.influxdata.com/downloads/) Open source time series database for storing data
    4. [Grafana v8.0.2](https://grafana.com/grafana/download?pg=get&plcmt=selfmanaged-box1-cta1) Dashboard for visualise the data
3. Errors and Fixes
    1. Serial port Permission - sudo chown username /dev/ttyUSB0
    2. [ESP emqx](https://www.emqx.io/blog/esp8266-connects-to-the-public-mqtt-broker) connection
    3. [EMQX Manual Setup](https://sma.im/how-to-manually-setup-emqx-cluster/)

## Ports Used
    1. EMQX - 18083 //port
    2. InfluxDB - 8086
    3. Grafana -3000

### Refereneces
    1. https://savjee.be/2020/02/home-energy-monitor-v2/
    2. https://surtrtech.com/2019/01/21/easy-measure-of-ac-voltage-using-arduino-and-zmpt101b/
    3. https://www.casler.org/wordpress/low-current-measurement-performance-of-the-sct013/

<img src="https://gitlab.com/anirudhvl/project-energy-monitor-v1/-/raw/main/img/1.jpg" width="500" />
<img src="https://gitlab.com/anirudhvl/project-energy-monitor-v1/-/raw/main/img/3.jpg" width="500" />
<img src="https://gitlab.com/anirudhvl/project-energy-monitor-v1/-/raw/main/img/2.jpg" width="500" />
